/**
* @function Chat
* @description Sisältää chatin toiminnallisuuden.
*/
(function(){
        var getNode = function(s){
            return document.querySelector(s);
 
        },
 
//tehdään nodet
        status = getNode('.chat-status span'),
        messages = getNode('.chat-messages'),
        textarea = getNode('.chat textarea'),
        chatName = getNode('.chat-name'),
   
// saa statuksen idle
        StatusDefault = status.textContent,
 
 
// status hetken päästä.
        setStatus= function(s){
            status.textContent = s;
 
// Status palautetaan oletukseen 4 sekunnin kuluttua
            if(s!== StatusDefault)
            {
                var delay = setTimeout(function(){
                    setStatus(StatusDefault);
                },4000);
            }
        };
       
// Yhdistetään serveriin
        try{
            var socket = io.connect('https://testausta-alijohtaja.c9users.io:8082');
        }catch(e)
        {
           }
 // Jos tosi -> Yhteys OK!
        if(socket !== undefined)
        {
//kuuntelee outputtia
            socket.on('output', function(data){
               
// Pistetään viestit näkyviin
               if(data.length)
               {
                for(var x=0;x<data.length; x=x+1)
                {
                    var message = document.createElement('div');
                    message.setAttribute('class','chat-message');
                    message.textContent =
                            data[x].name + ': ' + data[x].message;
                       
// Append, eli lisätään #chat-messagesiin
                       
                    messages.appendChild(message);
                    messages.insertBefore(
                            message, messages.lastChild
                    );
                    messages.scrollTop = messages.scrollHeight;
 
               }
 
            }
        });
 
//kuuntelee statusta
            socket.on('status',function(data){
                setStatus((typeof data === 'object')? data.message: data);
 
                if(data.clear === true )
                {
                    textarea.value = '';
                }
            });
 
//kuuntelee napinpainallusta
            textarea.addEventListener('keydown', function(event){
                var self = this,
                    name = chatName.value;
                   
// Pelkkä ENTER ilman SHIFTIÄ lähettää viestin
                   if(event.which === 13 && event.shiftKey == false)
                   {
                        socket.emit('input',{
                           name: name,
                           message:self.value
                        });
 
                        event.preventDefault();
                   }
            });
            $("#submit").click(function() {
                    socket.emit('input', {
                        name: chatName.value,
                        message: textarea.value
                    });
                });
               
            }
        })();