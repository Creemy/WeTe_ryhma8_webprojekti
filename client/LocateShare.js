/**
 * Jakaa käyttäjän sijainnin.
 * @param location_callback - Kutsuu käyttäjän sijainnin.
 * @param error_callback - Antaa virheilmoituksen, jos sijainti ei ole saatavilla.
 * @returns Palauttaa käyttäjän tiedot.
 * @namespace
 */
function initLocationSharing(location_callback, error_callback){
 
/**
 * Luo satunnaisen id:n.
 * @returns Palauttaa id:n.
 */
    function guid() {
        function s4() { return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16).substring(1);
        };
       
        return s4() + s4() + '-' + s4() + '-' + s4() + s4();
    }
    
/** Luo käyttäjälle id:n ja antaa tiedon käyttöjärjestelmästä */
    var userInfo = {
        id: guid(),
        name: 'Test' + (navigator.platform? ' ('+navigator.platform+')':'')
    }

// Socket io yhteys/setuppi
    var socket = io.connect('https://testausta-alijohtaja.c9users.io:8080');
                              
    socket.on('connect', function () {
        socket.on('location', function(location){
            if(location.id != userInfo.id) {
                location_callback(location);
            }
        })
    });
 
 
// paikannushaku
   
    if (!navigator.geolocation) {
        return userInfo;
    }

/**
 * Asettaa käyttäjän sijainnin.
 * @param position - Käyttäjän sijainti.
 */
    function geo_success(position) {
        var longitude = position.coords.longitude;
        userInfo.latitude  = position.coords.latitude;
        userInfo.longitude = position.coords.longitude;
       
        location_callback(userInfo);
        sendLocation();
    }

/** Antaa virheen, jos sijaintia ei löydy. */
    function geo_error() {
        error_callback();
    }
 
    var sendLocationTimeout = null;
    
/** Lähettää sijainnin serverille. */    
    function sendLocation(){
        socket.emit('location', userInfo);
        clearTimeout(sendLocationTimeout);
        sendLocationTimeout = setTimeout(sendLocation, 1000*5);
    }
 
    var geo_options = { enableHighAccuracy: true };
    navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
    navigator.geolocation.getCurrentPosition(geo_success, geo_error, geo_options);
 
    return userInfo;
}