/**
* @namespace Server
* @description Sisältää serverin toiminnallisuuden.
*/

//mappi yhteys socket.io:lla
var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
server.listen(process.env.PORT || 8080);
 
io.sockets.on('connection', function (socket) {
  socket.on('location', function (data) {
    io.sockets.emit('location', data);
  });
});
 
// eli kansio johon viitataan
app.use(require('express').static(__dirname + '/client'));
 
// tästä alkaa mongodb/socket.io yhteyschatille
//luodaan chatille database yhteys
// tarvitsee yhtyeden mongodb
var mongo = require('mongodb').MongoClient,
// tarvitsee socket.io
    client = require('socket.io').listen(8082).sockets;
 
    mongo.connect('mongodb://127.0.0.1/chat', function(err,db){
        if(err) throw err;
           
            // Kaikille yhteyksille (client)
          client.on('connection',function(socket){
            //colli viestit
            var col = db.collection('messages'),
           
            //funkiot joka lähettää selaimen statusmuutoksen
                sendStatus = function(s){
                  socket.emit('status',s);
                };
           
            // Emitta kaikki viestit ydhistyvään clienttiin
            // Enintään 100 viestiä
            // Järjestetään ID:n mukaan käänteisesti
                col.find().limit(100).sort({_id:1}).toArray(function(err,res){
                    if(err) throw err;
                    socket.emit('output',res);
                });
 
            //odottaa single clienttia
            socket.on('input', function(data){
               
            // Yksittäisen input-elementin attribuutit
                var name = data.name;
                var message = data.message;
 
               var whitespace = /^\s*$/;
             
               // tarkistaa onko tyhjä
                if(whitespace.test(name) || whitespace.test(message))
                {
                    sendStatus('Nimi ja Viesti pakollinen');
                }
                else
                {
                    //lisää dataa *collectioniin*
                    col.insert({name: name,message:message}, function(){
 
                        //emit latest messages to all clients
                        client.emit('output',[data]);
 
                        // tyhjää viestikentän
                        sendStatus({
                            message:"Viestisi lähetetty",
                            clear:true
                        });
                    });
                }
 
            });
         });
    });