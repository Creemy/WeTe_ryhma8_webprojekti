/** @namespace Map */

// luo listan markkereille ja id:lle
    var markers = [];
    var uniqueId = 1;


// antaa tiedot käyttäjästä
    var currentUserInfo = null;
    var users = {};
// google mapin tiedot
    var map = null;
    var infowindow = null;
    var refreshTimeout = null;
    
/**
 * Päivittää käyttäjän sijainnin
 * @param userInfo - Käyttäjän tiedot.
 * @param userInfo.id - Käyttäjän id.
 * @param userInfo.name - Käyttäjän nimi.
 * @param userInfo.latitude - Käyttäjän sijainnin leveysaste.
 * @param userInfo.longitude - Käyttäjän sijainnin pituusaste.
 * @memberof Map
 */
    function userLocationUpdate(userInfo){
        if(!users[userInfo.id]) users[userInfo.id] = { id: userInfo.id };
                
        users[userInfo.id].name = userInfo.name;
        users[userInfo.id].latitude  = userInfo.latitude;
        users[userInfo.id].longitude = userInfo.longitude;
        users[userInfo.id].timestamp = new Date().getTime();
        refreshMarkers();
    } 
    
/** 
 * Päivittää reaaliajassa käyttäjän tietoja ja muuttaa tarvittaessa koordinaatteja.
 * @memberof Map
 */
    function refreshMarkers(){
        if (!map) return;
        if (!currentUserInfo.movedMapCenter && currentUserInfo.timestamp) {
            $('#user-name').val(currentUserInfo.name);
            $('#user-name').bind('keyup', function() {
                currentUserInfo.name = $('#user-name').val();
            })
            currentUserInfo.movedMapCenter = true;
            map.setCenter(new google.maps.LatLng(
                currentUserInfo.latitude, currentUserInfo.longitude));
        }
        for (var id in users) {
            var userInfo = users[id];
            if(userInfo.marker){
    
// jos ei saada käyttäjältä tietoa 
//  poistetaan markkeri
                if( userInfo.id != currentUserInfo.id && 
                    userInfo.timestamp + 1000*30 < new Date().getTime() ){
                    userInfo.marker.setMap(null);
                    delete users[id];
                    continue;
                }
            }else{
                      
// tehdään uusi markkeri
        var marker = new google.maps.Marker({
            map:map,
            icon: {
                url: "image/orange-marker.png", // url
                scaledSize: new google.maps.Size(50, 50)
            }
        });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(marker.getTitle())
                    infowindow.open(map, marker);
                });
                userInfo.marker = marker;
            }
//siirrä markkeria
            userInfo.marker.setTitle(userInfo.name);
            userInfo.marker.setPosition( 
                new google.maps.LatLng(userInfo.latitude, userInfo.longitude));
        }
                
        $('#user-number').text(Math.max(Object.keys(users).length-1,0) +'')
                
// päivittää markkereita joka 20 sekuntti
        clearTimeout(refreshTimeout) 
        refreshTimeout = setTimeout(refreshMarkers, 1000*20);
    }

/**
 * Alustaa kartan. Alkukeskitys asetettu Helsinkiin.
 * @memberof Map
 */
    function mapInitialize() {
        map = new google.maps.Map(document.getElementById("map-canvas"), { 
            zoom: 10,
            center: new google.maps.LatLng(60.192059, 24.945831) 
        });
        infowindow = new google.maps.InfoWindow({ content: 'Test' });
        google.maps.event.addDomListener(map, 'click', function() {
            infowindow.close(map);
        });
        refreshMarkers();
                
        var geocoder = new google.maps.Geocoder();

        document.getElementById('addMarker').addEventListener('click', function() {
            geocodeAddress(geocoder, map);
        });
    }
    
/**
 * Keskittää kartan toisen käyttäjän sijaintiin ja avaa infoikkunan, jossa käyttäjän tiedot.
 * @memberof Map
 */
    function move_to_otheruser(){
        var ids = Object.keys(users);
        ids.slice(ids.indexOf(currentUserInfo.id),1);
        var random_user_id = ids[Math.floor(ids.length * Math.random())];
        var userInfo = users[random_user_id];
        map.setCenter(new google.maps.LatLng(userInfo.latitude, userInfo.longitude));
        infowindow.setContent(userInfo.name);
        infowindow.open(map, userInfo.marker);
    }
    
    google.maps.event.addDomListener(window, 'load', mapInitialize);
    currentUserInfo = initLocationSharing(userLocationUpdate);

//Markkerin lisääminen
/**
 * Lisää karttaan uuden markkerin.
 * @param geocoder - Google Maps APIn geokoodauspalvelu. Antaa maaantieteellisen koodin, jonka avulla tietty paikka tai alue voidaan tunnistaa.
 * @param resultsMap - Kartta, johon markkerit lisätään.
 * @memberof Map
 */
    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        var title = document.getElementById('marker-title').value;
                
        var infoWindow = new google.maps.InfoWindow();
                
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    title: title,
                    animation: google.maps.Animation.DROP,
                    icon: {
                        url: "image/green-marker.png", // url
                        scaledSize: new google.maps.Size(50, 50)
                        }
                });
                
//Markkeri saa yksilöllisen id:n
                marker.id = uniqueId;
                uniqueId++;
                
//Annetaan markkerille click event handleri
                google.maps.event.addListener(marker, "click", function (e) {
                    var content = '<p style="text-align:center;font-size:16px;">' + title + '</p>';
                    content += "<br /><input type = 'button' onclick = 'DeleteMarker(" + marker.id + ");' value = 'Delete' style='margin:auto;display:block;'/>";
                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infoWindow.open(map, marker);
                });

//Markerin lisääminen listaan
                markers.push(marker);
                
            } else {
                alert('Emme valitettavasti löytäneet sijaintiasia anna uudestaan: ' + status);
            }
        });
    }
 
/**
 * Etsii ja poistaa markkerin kartalta.
 * @param id - Poistettavan markkerin id-numero.
 * @memberof Map
 */   
    function DeleteMarker(id) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id == id) {
//Poistaa markerin  mapista               
                markers[i].setMap(null);
//Poistaa markerin listasta
                markers.splice(i, 1);
                return;
            }
        }
    };
        